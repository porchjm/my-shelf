import json
import sqlite3
import datetime

import data

book_data = {
    "1" : {
        "title": "Title1",
        "author": "Author1",
        "category": "Fiction",
        "isbn": "",
        "owned": "Library",
        "read": "Complete",
        "readDate": "Jan 2020",
    },
    "2" : {
        "title": "Title2",
        "author": "Author2",
        "category": "Literary Fiction",
        "isbn": "",
        "owned": "Library",
        "read": "In Progress",
        "readDate": "Jan 2020",
    },
}

data.Config.init()
data.User.init()
data.Book.init()

conn = sqlite3.connect("data.db")
c = conn.cursor()

data.User.add("Username","Token","true")

timestamp = datetime.datetime.now()
for key, book in book_data.items():
    data.Book.create(book)

print(data.Book.get_all())