import json
import falcon
import sqlite3

def is_authorized(token):
    if token is None:
        return False
    token = token.split("Bearer ")[1]

    conn = sqlite3.connect("data.db")
    conn.row_factory = sqlite3.Row
    c = conn.cursor()

    return c.execute("SELECT * FROM users WHERE token = ? and active='true'",(token,)).fetchone() is not None

def forbidden(resp):
    resp.body = json.dumps({"Error":"Not Authorized"})
    resp.status=falcon.HTTP_403

def block_on_unauth(req, resp):
    if not is_authorized(req.auth):
        forbidden(resp)
        return True
    return False
