import json
import falcon
from data import Book, Category, User
import auth

class book(object):
    def on_get(self, req, resp, bid):
        print(req)
        #TODO: Do this via middleware, not just copying the code to all requests
        resp.set_header('Access-Control-Allow-Origin', '*')
        book = Book.get(bid)
        if book is None:
            resp.body = json.dumps({"Error":"Book not found"})
            resp.status=falcon.HTTP_404
            print(resp)
            return
        resp.body = json.dumps(book)
        resp.status=falcon.HTTP_200
        print(resp, book)

    def on_post(self, req, resp, bid):
        print(req)
        resp.set_header('Access-Control-Allow-Origin', '*')
        if auth.block_on_unauth(req,resp):
            return
        book = Book.update(bid, req.media)
        if book is None:
            resp.body = json.dumps({"Error":"Failed to update"})
            resp.status=falcon.HTTP_400
            print(resp)
            return
        
        resp.body = json.dumps(book)
        resp.status=falcon.HTTP_200
        print(resp, book)

    def on_delete(self, req, resp, bid):
        print(req)
        resp.set_header('Access-Control-Allow-Origin', '*') 
        if auth.block_on_unauth(req,resp):
            return
        if Book.delete(bid) is None:
            resp.body = json.dumps({"Error":"Book not found"})
            resp.status=falcon.HTTP_404
            print(resp)
            return
        
        resp.body={}
        resp.status=falcon.HTTP_204
        print(resp)

class books(object):
    def on_get(self, req, resp):
        print(req)
        resp.set_header('Access-Control-Allow-Origin', '*')
        books = Book.get_all(req.query_string)
        resp.body = json.dumps(books)
        resp.status=falcon.HTTP_200
        print(resp, books)

    def on_post(self, req, resp):
        print(req)
        resp.set_header('Access-Control-Allow-Origin', '*')
        if auth.block_on_unauth(req,resp):
            return
        book = Book.create(req.media)
        if book is None:
            resp.body = json.dumps({"Error":"Need at least a title and author to add a book"})
            resp.status=falcon.HTTP_400
            print(resp)
            return
        if type(book) is not dict:
            resp.body = json.dumps({"Error":"Book already present, update it instead","ID":book})
            resp.status=falcon.HTTP_400
            print(resp)
            return

        resp.body = json.dumps(book)
        resp.status=falcon.HTTP_200
        print(resp, book)

class category(object):
    def on_get(self, req, resp, slug):
        print(req)
        #TODO: Do this via middleware, not just copying the code to all requests
        resp.set_header('Access-Control-Allow-Origin', '*')
        category = Category.get(slug)
        if category is None:
            resp.body = json.dumps({"Error":"Category not found"})
            resp.status=falcon.HTTP_404
            print(resp)
            return
        resp.body = json.dumps(category)
        resp.status=falcon.HTTP_200
        print(resp, category)

    def on_post(self, req, resp, slug):
        print(req)
        resp.set_header('Access-Control-Allow-Origin', '*')
        if auth.block_on_unauth(req,resp):
            return
        category = Category.update(slug, req.media)
        if category is None:
            resp.body = json.dumps({"Error":"Failed to update"})
            resp.status=falcon.HTTP_400
            print(resp)
            return
        
        resp.body = json.dumps(category)
        resp.status=falcon.HTTP_200
        print(resp, category)

    def on_delete(self, req, resp, slug):
        print(req)
        resp.set_header('Access-Control-Allow-Origin', '*') 
        if auth.block_on_unauth(req,resp):
            return
        if Category.delete(slug) is None:
            resp.body = json.dumps({"Error":"Category not found"})
            resp.status=falcon.HTTP_404
            print(resp)
            return
        
        resp.body={}
        resp.status=falcon.HTTP_204
        print(resp)

class categories(object):
    def on_get(self, req, resp):
        print(req)
        resp.set_header('Access-Control-Allow-Origin', '*')
        categories = Category.get_all()
        resp.body = json.dumps(categories)
        resp.status=falcon.HTTP_200
        print(resp, categories)

    def on_post(self, req, resp):
        print(req)
        resp.set_header('Access-Control-Allow-Origin', '*')
        if auth.block_on_unauth(req,resp):
            return
        category = Category.create(req.media)
        if category is None:
            resp.body = json.dumps({"Error":"Could not create Category"})
            resp.status=falcon.HTTP_400
            print(resp)
            return
        if type(category) is not dict:
            resp.body = json.dumps({"Error":"Category already present, update it instead","Slug":category})
            resp.status=falcon.HTTP_400
            print(resp)
            return

        resp.body = json.dumps(category)
        resp.status=falcon.HTTP_200
        print(resp, category)

class user(object):
    def on_get(self, req, resp, uid):
        print(req)
        resp.set_header('Access-Control-Allow-Origin', '*')
        user = User.get(uid)
        if user is None:
            resp.body = json.dumps({"Error":"User not found"})
            resp.status=falcon.HTTP_404
            print(resp)
            return
        resp.body = json.dumps(user)
        resp.status=falcon.HTTP_200
        print(resp, user)

class users(object):
    def on_get(self, req, resp):
        resp.set_header('Access-Control-Allow-Origin', '*')
        if auth.block_on_unauth(req,resp):
            return
        print(req)
        users = User.get_all()
        resp.body = json.dumps(users)
        resp.status=falcon.HTTP_200
        print(resp, users)