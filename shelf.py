import falcon
import json
import data
import routes

api = application = falcon.API()

api.add_route('/books', routes.books())
api.add_route('/books/{bid}',routes.book())
api.add_route('/categories', routes.categories())
api.add_route('/categories/{slug}',routes.category())