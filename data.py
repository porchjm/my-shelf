import json
import sqlite3
import datetime
import secrets
import urllib.parse
from hashids import Hashids

class Book(object):
    @staticmethod
    def init():
        """Initializes the Book object database. Will destroy any data currently stored in the books table of the SQLite datastore.
        """
        conn = sqlite3.connect("data.db")
        c = conn.cursor()

        c.execute("DROP TABLE IF EXISTS books;")
        c.execute("CREATE TABLE books(id text primary key, title text, author text, isbn text, owned_physical text, owned_digital text, lastUpdate text);")

        conn.commit()

    @staticmethod
    def get(id):
        """Returns a single book based on ID. Returns a JSON Book object if found, or None if not. Will only return private entries if a valid authorization token is provided.
        Parameter:
            id: alphanumeric string that identifies a specific Book object to be retrived
        """
        conn = sqlite3.connect("data.db")
        conn.row_factory = sqlite3.Row
        c = conn.cursor()
        i = c.execute("SELECT rowid, * FROM books WHERE id=?",(id,)).fetchone()
        if i is None:
            return i
        return {
            "rowid": i["rowid"],
            "id": i["id"],
            "title": i["title"],
            "author": i["author"],
            "categories": Book.get_categories(id),
            "isbn": i["isbn"],
            "owned_physical": i["owned_physical"],
            "owned_digital": i["owned_digital"],
            "lastUpdate": i["lastUpdate"],
        }

    @staticmethod
    def get_all(query=""):
        """Returns a list of all Book objects, optionally filtered by the provided query string
        Parameter:
            query: string in the URL query string format, containing &-separated list of field=value pairs.
        """
        whereClause = []
        filters = {}
        q = None
        if query is not "": 
            q = query.split("&")
            for filter in q:
                #TODO Whitelist specific filters/sanitize input
                key = filter.split("=")[0]
                value = filter.split("=")[1]
                # Initial implementation based on this source:
                # https://stackoverflow.com/questions/15496051/dynamic-sql-where-clause-generation
                whereClause.append("{} = :{}".format(key,key))
                filters[key] = urllib.parse.unquote(value)

        conn = sqlite3.connect("data.db")
        conn.row_factory = sqlite3.Row
        c = conn.cursor()
        data = []
        sql = "SELECT rowid, * FROM books"
        if q is not None:
            sql = "{} WHERE {}".format(sql," AND ".join(whereClause))

        res = c.execute(sql).fetchall() if q is None else c.execute(sql,filters).fetchall()
        for i in res:
            item = {
                "rowid": i["rowid"],
                "id": i["id"],
                "title": i["title"],
                "author": i["author"],
                "categories": Book.get_categories(i["id"]),
                "isbn": i["isbn"],
                "owned_physical": i["owned_physical"],
                "owned_digital": i["owned_digital"],
                "lastUpdate": i["lastUpdate"],
            }
            data.append(item)
        return data

    @staticmethod
    def create(book):
        """Creates a new Book using the provided information. Returns the Book if successful, None if invalid, and just an ID if that Book already exists
        Parameter:
            book: Dictionary containing data for a new Book object. Must contain at least "title" and "author" fields. Any fields that don't map to a Book will be ignored 
        """
        if book.get("title") is None or book.get("author") is None:
            return None 
        conn = sqlite3.connect("data.db")
        conn.row_factory = sqlite3.Row
        c = conn.cursor()

        # Don't allow multiple entries of a Title by the same Author
        alreadyExists = c.execute("SELECT id FROM books WHERE title=? AND author=?",(book.get("title"),book.get("author"))).fetchone()
        if alreadyExists is not None:
            return alreadyExists[0]
        
        # Don't allow duplicate ISBNs
        if book.get("isbn","") != "":
            alreadyExists = c.execute("SELECT id FROM books WHERE isbn=?",(book.get("isbn"),)).fetchone()
            if alreadyExists is not None:
                return alreadyExists[0]

        hashSalt = c.execute("SELECT salt FROM config").fetchone()[0] + "_books"

        prevID = c.execute("SELECT MAX(rowid) FROM books").fetchone()[0] or 0
        newID = Hashids(salt=hashSalt,min_length=8,alphabet="abcdefghijklmnopqrstuvwxyz1234567890").encode(prevID)

        title = book.get("title")
        author = book.get("author")
        isbn = book.get("isbn","")
        owned_physical = book.get("owned_physical","")
        owned_digital = book.get("owned_digital","")

        c.execute("INSERT INTO books VALUES (?,?,?,?,?,?,?)",(newID,title,author,isbn,owned_physical,owned_digital,datetime.datetime.now()))
        conn.commit()

        return Book.get(newID)

    @staticmethod
    def update(id, book):
        """Updates Book specified by the provided ID using the data provided
        Parameter:
            id: alphanumeric string that identifies a specific Book object to be updated
            book: Dictionary containing data for a new Book object. Any fields that don't map to a Book will be ignored 
        """
        conn = sqlite3.connect("data.db")
        conn.row_factory = sqlite3.Row
        c = conn.cursor()

        oldBook = Book.get(id)

        title = book.get("title",oldBook["title"])
        author = book.get("author",oldBook["author"])
        isbn = book.get("isbn",oldBook["isbn"])
        owned_physical = book.get("owned_physical",oldBook["owned_physical"])
        owned_digital = book.get("owned_digital",oldBook["owned_digital"])

        c.execute("UPDATE books SET title=?,author=?,isbn=?,owned_physical=?,owned_digital=?,lastUpdate=? WHERE id=?",(title,author,isbn,owned_physical,owned_digital,datetime.datetime.now(),id))
        conn.commit()

        return Book.get(id)

    @staticmethod
    def delete(id):
        """Deletes the Book specified by the provided ID. Returns 204 to indicate a successful removal or None if no book is found.
        Parameter:
            id: alphanumeric string that identifies a specific Book object to be deleted
        """
        if Book.get(id) is None:
            return None
        conn = sqlite3.connect("data.db")
        conn.row_factory = sqlite3.Row
        c = conn.cursor()

        c.execute("DELETE FROM books WHERE id=?",(id,))
        conn.commit()

        return 204

    @staticmethod
    def get_categories(id):
        """Returns an array of all Categories the specified Books has been assigned.
        Parameter:
            id: alphanumeric string that identifies a specific Book to find the Categories for
        """
        conn = sqlite3.connect("data.db")
        conn.row_factory = sqlite3.Row
        c = conn.cursor()

        cats = c.execute("SELECT categorySlug FROM book_categories WHERE bookID=?",(id,)).fetchall()
        categories = []
        for cat in cats:
            categories.append(Category.get(cat["categorySlug"])["name"])
        
        return categories

class Config(object):
    @staticmethod
    def init(salt=None):
        """Initializes the Configuration database. Will destroy any data currently stored in the config table of the SQLite datastore.
        """
        conn = sqlite3.connect("data.db")
        c = conn.cursor()
        if salt is None:
            salt = input("ID Salt? ")

        c.execute("DROP TABLE IF EXISTS config")
        c.execute("CREATE TABLE config(salt string)")
        c.execute("INSERT INTO config VALUES (?)", (salt,))
        conn.commit()

class User(object):
    @staticmethod 
    def init():
        """Initializes the User database. Will destroy any data currently stored in the users table of the SQLite datastore.
        """
        conn = sqlite3.connect("data.db")
        c = conn.cursor()

        c.execute("DROP TABLE IF EXISTS users;")
        c.execute("CREATE TABLE users(id text primary key, name string UNIQUE, token string, active string);")
        conn.commit()

    @staticmethod
    def add(name, active="false", setToken=None):
        """Inserts a new User into the users table. Returns the new User on success and None on failure.
        Parameters:
            name (Required): the username for the new User. Must not duplicate any existing username.
            active: "true" if the User should be considered active for authorization purposes. Any other value is considered false. Default value: "false"
            setToken: If provided, this value is used for the token associated with new User. If not provided, a new URL-safe token will be generated randomly.
        """
        conn = sqlite3.connect("data.db")
        c = conn.cursor()

        if name is None:
            return None

        hashSalt = c.execute("SELECT salt FROM config").fetchone()[0] + "_users"

        oldID = c.execute("SELECT MAX(rowid) FROM users;").fetchone()[0] or 0
        newID = Hashids(salt=hashSalt,min_length=8,alphabet="abcdefghijklmnopqrstuvwxyz1234567890").encode(oldID)

        token = setToken or secrets.token_urlsafe()
        c.execute("INSERT INTO users VALUES (?,?,?,?)",(newID,name,token,active))
        conn.commit()
        return User.get(newID)

    @staticmethod
    def get(id):
        """Returns the User specified by the provided id. Will return a JSON User object if found, or None if not.
        Parameter:
            id: alphanumeric string that identifies a specific Book object to be retrived
        """
        conn = sqlite3.connect("data.db")
        conn.row_factory = sqlite3.Row
        c = conn.cursor()
        i = c.execute("SELECT rowid, * FROM users WHERE id = ?",(id,)).fetchone()

        if i is None:
            return i
        return {
            "rowid": i["rowid"],
            "id": i["id"],
            "name": i["name"],
            "token": i["token"]
        }

    @staticmethod
    def get_all():
        conn = sqlite3.connect("data.db")
        conn.row_factory = sqlite3.Row
        c = conn.cursor()
        i = c.execute("SELECT rowid, * FROM users",(id,)).fetchone()
        
        data = []
        for i in c.execute("SELECT rowid, * FROM books").fetchall():
            item = {
                "rowid": i["rowid"],
                "id": i["id"],
                "name": i["name"],
                "token": i["token"]
            }
            data.append(item)
        return data

class Category(object):
    @staticmethod 
    def init():
        """Initializes the Category database. Will destroy any data currently stored in the categories table of the SQLite datastore.
        """
        conn = sqlite3.connect("data.db")
        c = conn.cursor()

        c.execute("DROP TABLE IF EXISTS categories")
        c.execute("CREATE TABLE categories(slug text primary key, name string UNIQUE, active text, lastUpdate text)")
        conn.commit()

        c.execute("DROP TABLE IF EXISTS book_categories")
        c.execute("CREATE TABLE book_categories(bookID text, categorySlug text)")
        conn.commit()

    @staticmethod
    def create(category):
        """Inserts a new Category into the categories table. Returns the new Category on success, just a slug if the Category already exists, and None on an error.
        Parameters:
            slug (Required): shorthand unique identifier for the Category
            name: user-friendly value for display. If omitted, the slug will be used.
            active: indicates if the Category should show up as an option or not. Accepted values are "true" and "false"
        """
        conn = sqlite3.connect("data.db")
        c = conn.cursor()

        slug = category.get("slug")
        if slug is None:
            return None
        
        name = category.get("name",slug)
        active = category.get("active","true")

        existingCategory = c.execute("SELECT slug FROM categories WHERE slug = ? UNION SELECT slug FROM categories WHERE name = ?",(slug,name)).fetchone()
        if existingCategory is not None:
            return existingCategory[0]

        c.execute("INSERT INTO categories VALUES (?,?,?,?)",(slug,name,active,datetime.datetime.now()))
        conn.commit()

        return Category.get(slug)

    @staticmethod
    def get(slug):
        """Returns the Category specified by the provided slug. Will return a JSON Category object if found, or None if not.
        Parameter:
            slug: alphanumeric string that identifies a specific Category object to be retrived
        """
        conn = sqlite3.connect("data.db")
        conn.row_factory = sqlite3.Row
        c = conn.cursor()
        i = c.execute("SELECT rowid, * FROM categories WHERE slug = ?",(slug,)).fetchone()

        if i is None:
            return i
        return {
            "rowid": i["rowid"],
            "slug": i["slug"],
            "name": i["name"],
            "active": i["active"],
            "lastUpdate": i["lastUpdate"],
        }

    @staticmethod
    def get_all():
        conn = sqlite3.connect("data.db")
        conn.row_factory = sqlite3.Row
        c = conn.cursor()
        
        data = []
        for i in c.execute("SELECT rowid, * FROM categories").fetchall():
            item = {
                "rowid": i["rowid"],
                "slug": i["slug"],
                "name": i["name"],
                "active": i["active"],
                "lastUpdate": i["lastUpdate"],
            }
            data.append(item)
        return data

    @staticmethod
    def update(slug, category):
        """Updates Category specified by the provided slug using the data provided
        Parameter:
            slug: alphanumeric string that identifies a specific Category object to be updated
            category: Dictionary containing data for a new Category object. Any fields that don't map to a Category will be ignored 
        """
        conn = sqlite3.connect("data.db")
        conn.row_factory = sqlite3.Row
        c = conn.cursor()

        oldCategory = Category.get(slug)

        if oldCategory is None:
            return None

        name = category.get("name",oldCategory["name"])
        active = category.get("active",oldCategory["active"])

        c.execute("UPDATE categories SET name=?,active=?,lastUpdate=? WHERE slug=?",(name,active,datetime.datetime.now(),slug))
        conn.commit()

        return Category.get(slug)

    @staticmethod
    def assign(bookID, categorySlug):
        """Records that given category applies to specified book. Returns Book that has been categorized if successful, None if there's an error.
        Parameters:
            bookID: the identifier for the intended book
            categorySlug: the identifier for the intended category
        """
        category = Category.get(categorySlug)
        book = Book.get(bookID)
        if  category is None or book is None:
            return None

        conn = sqlite3.connect("data.db")
        conn.row_factory = sqlite3.Row
        c = conn.cursor()

        if c.execute("SELECT * FROM book_categories WHERE bookID=? AND categorySlug=?",(bookID, categorySlug)).fetchone() is not None:
            return book

        c.execute("INSERT INTO book_categories VALUES (?,?)", (bookID, categorySlug))
        conn.commit()

        return book

    @staticmethod
    def unassign(bookID, categorySlug):
        """Removes an existing link betwen specified Book and Category. Returns 204 to indicate a successful removal or None if no Cateogry link is found.
        Parameters:
            bookID: the identifier for the intended book
            categorySlug: the identifier for the intended category
        """
        category = Category.get(categorySlug)
        book = Book.get(bookID)
        if  category is None or book is None:
            return None

        conn = sqlite3.connect("data.db")
        conn.row_factory = sqlite3.Row
        c = conn.cursor()

        if c.execute("SELECT * FROM book_categories WHERE bookID=? AND categorySlug=?",(bookID, categorySlug)).fetchone() is not None:
            c.execute("DELETE FROM book_categories WHERE bookID=? AND categorySlug=?",(bookID,categorySlug))
            conn.commit()
            return 204
        
        return None

    @staticmethod
    def delete(slug):
        """Deletes the Category specified by the provided slug. Returns 204 to indicate a successful removal or None if no Cateogry is found.
        Parameter:
            slug: alphanumeric string that identifies a specific Category object to be deleted
        """
        if Category.get(slug) is None:
            return None

        conn = sqlite3.connect("data.db")
        conn.row_factory = sqlite3.Row
        c = conn.cursor()

        c.execute("DELETE FROM categories WHERE slug=?",(slug,))
        conn.commit()

        return 204